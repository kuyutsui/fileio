package FileIO;

import java.util.*;
import java.io.*;
import java.lang.*;

public class FileIO {
	File f;	

	public FileIO(String i_file) {
		this.f = new File(i_file);
	}
	
	public void getFileProperties() {
		System.out.println("File name : " + this.f.getName());
		System.out.println("Path: " + this.f.getPath());
		System.out.println("Exists :" + this.f.exists());
	}

	public ArrayList<person> fileInputStream() {
		ArrayList<person> person = new ArrayList<person>();
		String inLine;
		person p;
		int ID = 1001;
		try {
			BufferedReader fin = new BufferedReader(new FileReader(this.f.getPath()));
			
			while((inLine = fin.readLine()) != null) {
				person.add(new person(inLine));
				person.get(person.size() - 1).setID(ID + person.size() - 1);
			}
						
			fin.close();
		}catch(IOException e) {
			System.out.println(e);
		}
		
		return person;
	}
	
	public void fileOutputStream(String fileName, String data, boolean notOveride) {
		try {
			BufferedWriter buffer = new BufferedWriter(new FileWriter(fileName, notOveride));
			buffer.write(data);
			buffer.newLine();
			buffer.close();
		}catch (Exception e) {
			System.out.println(e);
		}
	}
}
